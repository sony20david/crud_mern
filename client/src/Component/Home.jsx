import { AppBar, Toolbar, makeStyles } from '@material-ui/core';
import { NavLink } from 'react-router-dom';

const useStyle = makeStyles({
    header: {
        background: '#111111'
    },
    tabs: {
        color: '#FFFFFF',
        marginRight: 20,
        textDecoration: 'none',
        fontSize: 20
    }
})

const Home = () => {
    const classes = useStyle();
    return (
        <>
        <div className='center'>
        <h1>WELCOME TO BOOK STORE</h1>
        </div>
        </>
    )
}

export default Home;