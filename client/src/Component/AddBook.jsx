import react, { useState } from 'react';
import { FormGroup, FormControl, InputLabel, Input, Button, makeStyles, Typography } from '@material-ui/core';
import { addUser } from '../Service/api';
import { useHistory } from 'react-router-dom';

const initialValue = {
    bookname: '',
    book_description: '',
    book_price: '',
    book_discount: '',
    book_availability:'',
    book_author:'',
    book_publisher:''
}

const useStyles = makeStyles({
    container: {
        width: '50%',
        margin: '5% 0 0 25%',
        '& > *': {
            marginTop: 20
        }
    }
})

const AddBook = () => {
    const [user, setUser] = useState(initialValue);
    const { bookname, book_description, book_price, book_discount ,book_availability,book_author,book_publisher} = user;
    const classes = useStyles();
    let history = useHistory();

    // const onValueChange = (e) => {
    //     console.log(e.target.value);
    //     setUser({...user, [e.target.bookname]: e.target.value})
    // }

    const addUserDetails = async() => {
        await addUser(user);
        history.push('./all');
    }

    return (
        <FormGroup className={classes.container}>
            <Typography variant="h4">Add Book</Typography>
            <FormControl>
                <InputLabel htmlFor="my-input">Name</InputLabel>
                <Input onChange={(e) => setUser({...user,bookname:e.target.value})} name='bookname' value={user.bookname} id="my-input" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">Description</InputLabel>
                <Input onChange={(e) => setUser({...user,book_description:e.target.value})} name='description' value={user.book_description} id="my-input" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">price</InputLabel>
                <Input onChange={(e) => setUser({...user,book_price:e.target.value})} name='price' value={user.book_price} id="my-input"/>
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">discount</InputLabel>
                <Input onChange={(e) => setUser({...user,book_discount:e.target.value})} name='discount' value={user.book_discount} id="my-input" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">availability</InputLabel>
                <Input onChange={(e) => setUser({...user,book_availability:e.target.value})} name='availability' value={user.book_availability} id="my-input" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">author</InputLabel>
                <Input onChange={(e) => setUser({...user,book_author:e.target.value})} name='author' value={user.book_author} id="my-input" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">publisher</InputLabel>
                <Input onChange={(e) => setUser({...user,book_publisher:e.target.value})} name='publisher' value={user.book_publisher} id="my-input" />
            </FormControl>
            <FormControl>
                <Button variant="contained" color="primary" onClick={() => addUserDetails()}>Add Book</Button>
            </FormControl>
        </FormGroup>
    )
}

export default AddBook;