import react, { useState, useEffect } from 'react';
import { Table, TableHead, TableCell, Paper, TableRow, TableBody, Button, makeStyles } from '@material-ui/core'
import { getUsers, deleteUser } from '../Service/api';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
    table: {
        width: '90%',
        margin: '50px 0 0 50px'
    },
    thead: {
        '& > *': {
            fontSize: 20,
            background: '#000000',
            color: '#FFFFFF'
        }
    },
    row: {
        '& > *': {
            fontSize: 18
        }
    }
})


const AllBook = () => {
    const [users, setUsers] = useState([]);
    const classes = useStyles();

    useEffect(() => {
        getAllUsers();
    }, []);

    const deleteUserData = async (id) => {
        await deleteUser(id);
        getAllUsers();
    }

    const getAllUsers = async () => {
        let response = await getUsers();
        setUsers(response.data);
    }

    return (
        <Table className={classes.table}>
            <TableHead>
                <TableRow className={classes.thead}>
                    <TableCell>Id</TableCell>
                    <TableCell>bookname</TableCell>
                    <TableCell>book_description</TableCell>
                    <TableCell>book_price</TableCell>
                    <TableCell>book_discount</TableCell>
                    <TableCell>book_availability</TableCell>
                    <TableCell>book_author</TableCell>
                    <TableCell>book_publisher</TableCell>
                    <TableCell></TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {users.map((user) => (
                    <TableRow className={classes.row} key={user.id}>
                        <TableCell>{user._id}</TableCell> {/* change it to user.id to use JSON Server */}
                        <TableCell>{user.bookname}</TableCell>
                        <TableCell>{user.book_description}</TableCell>
                        <TableCell>{user.book_price}</TableCell>
                        <TableCell>{user.book_discount}</TableCell>
                        <TableCell>{user.book_availability}</TableCell>
                        <TableCell>{user.book_author}</TableCell>
                        <TableCell>{user.book_publisher}</TableCell>
                        <TableCell>
                            <Button color="primary" variant="contained" style={{marginRight:10}} component={Link} to={`/edit/${user._id}`}>Edit</Button> {/* change it to user.id to use JSON Server */}
                            </TableCell>
                        <TableCell>
                            <Button color="secondary" variant="contained" onClick={() => deleteUserData(user._id)}>Delete</Button> {/* change it to user.id to use JSON Server */}
                            </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    )
}

export default AllBook;