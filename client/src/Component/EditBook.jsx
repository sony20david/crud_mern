import { useState, useEffect } from 'react';
import { FormGroup, FormControl, InputLabel, Input, Button, makeStyles, Typography } from '@material-ui/core';
import { useHistory, useParams } from 'react-router-dom';
import { getUsers, editUser } from '../Service/api';

const initialValue = {
    bookname: '',
    book_description: '',
    book_price: '',
    book_discount: '',
    book_availability:'',
    book_author:'',
    book_publisher:''
}

const useStyles = makeStyles({
    container: {
        width: '50%',
        margin: '5% 0 0 25%',
        '& > *': {
            marginTop: 20
        }
    }
})

const EditBook = () => {
    const [user, setUser] = useState(initialValue);
    const { bookname, book_description, book_price, book_discount ,book_availability,book_author,book_publisher } = user;
    const { id } = useParams();
    const classes = useStyles();
    let history = useHistory();

    useEffect(() => {
        loadUserDetails();
    }, []);

    const loadUserDetails = async() => {
        const response = await getUsers(id);
        setUser(response.data);
    }

    const editUserDetails = async() => {
        const response = await editUser(id, user);
        history.push('/all');
    }

    // const onValueChange = (e) => {
    //     console.log(e.target.value);
    //     setUser({...user, [e.target.name]: e.target.value})
    // }

    return (
        <FormGroup className={classes.container}>
            <Typography variant="h4">Edit Bookdetails</Typography>
            <FormControl>
                <InputLabel htmlFor="my-input">bookname</InputLabel>
                <Input onChange={(e) => setUser({...user,bookname:e.target.value})} name='name' value={bookname} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_desciption</InputLabel>
                <Input onChange={(e) => setUser({...user,book_description:e.target.value})} name='username' value={book_description} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_price</InputLabel>
                <Input onChange={(e) => setUser({...user,book_price:e.target.value})} name='email' value={book_price} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_discount</InputLabel>
                <Input onChange={(e) => setUser({...user,book_discount:e.target.value})} name='phone' value={book_discount} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_availability</InputLabel>
                <Input onChange={(e) => setUser({...user,book_availability:e.target.value})} name='phone' value={book_availability} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_author</InputLabel>
                <Input onChange={(e) => setUser({...user,book_author:e.target.value})} name='phone' value={book_author} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="my-input">book_publisher</InputLabel>
                <Input onChange={(e) => setUser({...user,book_publisher:e.target.value})} name='phone' value={book_publisher} id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
                <Button variant="contained" color="primary" onClick={() => editUserDetails()}>Edit Book</Button>
            </FormControl>
        </FormGroup>
    )
}

export default EditBook;